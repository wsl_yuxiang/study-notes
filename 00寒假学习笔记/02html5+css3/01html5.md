#		HTML5

增加了新特性:语义特性,本地存储特性,设备兼容特性,连接特性,网页多媒体特性,三维\图形及特效特性,性能与集成特性,CSS3特性.

  **广义HTML5**

HTML5本身+CSS3+JavaScript

###		1.HTML5新增语义化标签

![image-20200201171914597](D:\Documnt\01前端学习\06寒假学习笔记\html5+css3\images\image-20200201171914597.png)

###		2.H5新增多媒体标签

```html
音频 <audio></audio>
视频 <video></video>
```



谷歌浏览器把视频的自动播放给禁止了

```html
<video autoplay="autoplay" muted="muted" loop="loop" ></video>
```

muted="muted"将视频静音.

###		3.表单属性

```html
<input type="email">等等
```



| 属性         | 值        | 描述                                                         |
| ------------ | --------- | ------------------------------------------------------------ |
| required     | required  | 表单拥有该属性表示其内容不能为空,必填                        |
| placeholder  | 提示文本  | 表单的提示信息,存在默认值将不显示                            |
| autofocus    | autofocus | 自动聚焦属性,页面加载完成自动聚焦到指定表单                  |
| autocomplete | off/on    | 当用户在字段开始键入时,浏览器基于之前键入过的值,应该显示在字段中填写的选项,默认已经打开<br />需要表单内同时加上name属性,同时成功提交 |
| multiple     | multiple  | 可以多选文件提交                                             |





#		CSS3

###		1.CSS3属性选择器

* E[att] 选择具有att属性的E元素
* E[att="val"] 选择具有att属性且属性值等于val的E元素
* E[att^="val"] 选择具有att属性,属性值前三个字母为val的元素
* E[att$="val"] 选择具有att属性,且结尾为val值的元素
* E[att*="val"] 选择属性att里面含有"val"字段的元素



类选择器,属性选择器和伪类选择器的权重是10.

###		2.CSS3结构伪类选择器

| 选择符           | 简介                                           |
| ---------------- | ---------------------------------------------- |
| E:first-child    | 匹配父元素中的第一个子元素E                    |
| E:last-child     | 匹配父元素中最后一个E元素                      |
| E:nth-child(n)   | 匹配父元素中的第n个子元素E n是公式,但是从0开始 |
| E:first-of-type  | 指定类型E的第一个                              |
| E:last-of-type   | 指定类型E的最后一个                            |
| E:nth-of-type(n) | 指定类型E的第n个                               |



**n可选关键词:**

* even 偶数 
* odd 奇数

###		3.伪元素选择器

| 选择符   | 简介                     |
| -------- | ------------------------ |
| ::before | 在元素内的前面插入内容   |
| ::after  | 在元素内部的后面插入内容 |

**注意:**

1. before和after必须有content属性
2. before在内容的前面,after在内容的后面
3. before和after创建一个元素,但是属于行内元素
4. 在dom里面看不见刚刚创建的元素,所以称之为伪元素
5. 伪元素和标签选择器一样,权重为1



