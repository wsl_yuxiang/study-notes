###		Promise对象

####	promise三种状态

1. pending 就绪状态
2. resolved 成功状态
3. rejected 失败状态



状态变化如下:

prending =>resolved

prending =>rejected



####		基本使用

```javascript
 // 三种状态
    var promise = new Promise(function(resolved, rejected) {

            var num = Math.random() * 50;
            if (num < 30) {
                resolved(num);
            } else {
                rejected("太大了额");
            }
        })
        //后续函数
    promise.then(data => data, err => err).then(data => console.log(data + "sec"), err => console.log(data + "err"));
```

####		2.实例方法Promise.prototype.then



####		3.all和race方法

```javascript
 //all方法使用
    var p1 = new Promise(function(resolved, rejected) {

        var num = Math.random() * 50;
        resolved(num + 'p1');
    })
    var p2 = new Promise(function(resolved, rejected) {

        var num = Math.random() * 50;
        resolved(num + 'p2');
    })
    var p3 = new Promise(function(resolved, rejected) {

        var num = Math.random() * 50;
        resolved(num + 'p3');
    })

    //all 全部p1 p2 p3对象的状态都是resolved
    //race 有一个promise对象的状态是resolved就可以了
    Promise.all([p1, p2, p3]).then(data => console.log(data), err => console.log(err));
```

####		4.封装常用方法

![image-20200614123200396](D:\Documnt\01前端学习\06寒假学习笔记\04JS高级\images\image-20200614123200396.png)