###		1.元素的显示和隐藏

####		1.1 display显示

```html
display:none 属性值为none时为属性隐藏,隐藏之后,不在保留位置
display:block 显示隐藏
```



####		1.2visibility可见性

```html
visibility:inherit|visible|collapse|hidden
```

inherit:继承上一个父对象的可见性

visible:对象可视

hidden:对象隐藏

collapse:主要用来隐藏表格的行或列.

**隐藏之后位置还留着**

####		1.3overflow 溢出

当对象的内容超过其指定高度及宽度时如何管理内容

| 属性值  | 描述                                    |
| ------- | --------------------------------------- |
| visible | 不剪切内容也不添加滚动条                |
| hidden  | 不显示超过对象尺寸的内容,超出部分隐藏掉 |
| scroll  | 不管超出内容否,总是显示滚动条           |
| auto    | 超出自动显示滚动条,不超出不显示滚动条   |



###		2.鼠标的样式cursor

设置或检索在对象上移动的鼠标指针采用何种系统预定义的光标形状

| 属性值      | 描述      |
| ----------- | --------- |
| Default     | 小白,默认 |
| pointer     | 小手      |
| move        | 移动      |
| text        | 文本      |
| not-allowed | 禁止      |



语法:

```html
<ul>
    <li style="cursor:pointer"></li>
</ul>
```

###	3.轮廓线

取消轮廓线

```html
outline:none
```



###		4.居中对齐vertical-align

| 属性值   | 含义     |
| -------- | -------- |
| baseline | 基线对齐 |
| middle   | 居中对齐 |
| top      | 顶部对齐 |
| bottom   | 底部对齐 |

解决图片把盒子撑大的问题

1. 使用对齐不是基线对齐的方式

```html
vertical-align:top/bottom/middle
```

2. 给img添加display:block;转换为块级元素



###		4.溢出的文字省略号显示

####		4.1 white-space

```html
white-space:normal;默认处理方式
white-space:nowrap:强制在同一行内显示所有文本,直到文本结束或者遭遇br标签对象才换行
```



####		4.2 text-overflow

```html
text-overflow:clip;不显示省略标记(...),而是简单的裁切
text-overflow:ellipsis;当对象内文本溢出时显示省略标记(...)
```

注意:一定要首先强制一行内显示,再和overflow属性 搭配使用



```html
三部曲:
1.先强制一行内显示文本
white-space:nowrap;
2.超过的部分隐藏
overflow:hidden
3.文字用省略号代替超出的部分
text-overflow:ellipsis;

```

###		5.精灵技术

为什么要使用精灵技术

> 为了有效地减少服务器接收和发送请求的次数,提高页面的加载速度.

css精灵技术,css sprites, css 雪碧

####		5.1精灵技术

css精灵技术其实是将网页中的一些背景图片整合到一张大图中(精灵图),然而,各个网页元素通常只需要精灵图中不同位置的某个小图 

步骤:

1. 精确测量每个精灵图的大小和位置
2. 使用background-position:-100px -100px对背景图片定位,使图片定位

