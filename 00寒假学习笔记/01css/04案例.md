###		CSS书写顺序



1. 布局定位属性:display/position/float/clear/visibility/overflow 建议display第一个写
2. 自身属性:盒子里面的属性 width/height/margin/padding/border/background
3. 文本属性:color/font/text-decoration/text-algin/vertical-align/white-space/bread-word
4. 其他属性(CSS3):content/cursor/border-radius/box-shadow/text-shadow/backgroud/linear-gradient



###		布局流程

1. 必须确定页面的版心(可视区)
2. 分析页面中的行模块,以及每个行模块中的列模块.
3. 制作HTML结构,遵循先有结构后有样式的原则.结构永远最重要
4. 通过DIV+CSS布局来控制网页的各个模块



