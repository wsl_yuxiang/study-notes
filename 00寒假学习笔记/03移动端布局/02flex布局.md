flex 是 flexible box的缩写用来为盒妆模型提供最大的灵活性.无论是块还是行内元素都可以用

子容器可以横向排列,也可以纵向排列



####		常用父项属性

* flex-direction :设置主轴的方向
* justify-content:设置主轴上的子元素排列方式
* flex-wrap :设置子元素是否换行
* align-items:设置侧轴上的子元素排列方式(单行)
* align-content:设置侧轴上的子元素排列方式(多行)
* flex-flow:复合属性,相当于同时设置了flex-direction和flex-wrap



###		1.flex-direction

默认是x轴

```css
flex-direction: column;
```



####	2.justify-content属性

设置主轴上子元素的排列关系

默认是flex-start

```css
/* 内容元素排列 */
/* 从左到右 */
/* justify-content: flex-start; */
/* 居中对齐 */
/* justify-content: center; */
/* 平均空白空间 */
/* justify-content: space-around; */
/* 先两边贴边,平均剩余空间 */
justify-content: space-between;
```

####	3.flex-wrap属性

设置子元素是否换行

```css
/* 另起一行显示 */
flex-wrap: wrap;
```

####		4.align-items属性(单行)

侧轴的排列关系

```css
/* 侧轴居中对齐 */
align-items: center;
```

同时设置主轴和侧轴居中对齐,就可以做到垂直居中对齐

####		5.align-content(多行)

设置侧轴上的子元素的排列方式

![image-20200522172558231](D:\Documnt\01前端学习\06寒假学习笔记\03flex流式布局\images\image-20200522172558231.png)





#####		简写方式

```css
/* 简写方式 */
flex-flow: row wrap;
```



####		常见子项属性

* flex 子项目占的份数
* align-self 控制子项自己在侧轴上的排列方式
* order 属性定义子项的排列顺序(前后顺序)



####		1.flex属性

```css
section div:nth-child(2) {
    /* flex是第二个盒子占据空白部分 */
    /* 除去占有位置的盒子之外,其他的为剩余空间,1代表将空余空间设置为1份 */
    flex: 1;
    background-color: coral;
}
```



####		2.align-self 

控制子项自己在侧轴上的排列方式

```css
section div:nth-child(3) {
      width: 100px;
      height: 150px;
      background-color: skyblue;
      /* 子盒子侧轴的排序方式 */
      /* 自己往下走 */
      align-self: flex-end;
}
```

####		3.order

属性定义项目的排列顺序

数字越小,排列越靠前,默认为0

注意:跟z-index不一样





####		常见的布局思路

1.上下布局方式



![image-20200523150102391](E:\00Document\01学习\01前端学习\02学习笔记\00寒假学习笔记\03移动端布局\images\image-20200522172558231.png)



####		背景渐变色

语法:

```css
background: linear-gradient(起始方向,颜色1,颜色2,颜色3...);
```

背景颜色需要添加浏览器私有前缀 -webkit-

起始方向可以是方位名词或者度数,如果省略,默认是**top**



