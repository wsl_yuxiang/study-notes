###		维护css的弊端

* css需要书写大量看似没有逻辑的代码,css冗余度比较高
* 不方便维护及扩展,不利于复用
* css没有很好的计算能力



###   1.less简介

less(Leaner style sheets)是一门css扩展语言,也可以成为css预处理器

常见的css预处理器:sass less stylus



###		1.1less变量

```less
@变量名:值;
```

变量命名

* 前面是@开头
* @之后第一个字符不要加数字,不要连用两个@
* 区分大小写



###		1.2less编译

less文件需要编辑成css文件,才能使用





###		1.3less嵌套

可以在父元素的样式中,直接嵌套子元素的样式

```less
.header{
    width: 20px;
    height: 50px;
    background-color: pink;

    a {
        background-color: skyblue;
    }
}
```

伪类选择器,交集选择,伪元素选择器

* 加一个&,不加的话就当做子类选择器了
* 也可以在父类直接加a:hover(伪类选择器)

```less
a {
        background-color: skyblue;
        &:hover {
            color: red;
        }
    }
    a:hover{
        color: green;
    }
}
```



###		1.4less运算

less提供加减乘除运算

* <font color="red">乘号(*)和除号(/)</font>的写法
* <font color='red'>运算符中间左右有个空格隔开</font>
* 对于两个不同单位的值之间的运算,运算结果的值去第一个值的单位
* 如果两个值之间只有一个值有单位,则运算结果就该取该单位



##		2.rem适配方案

###		2.1方案1

* less
* 媒体查询
* rem



1. 首先选一套标准尺寸 750为准
2. 我们用屏幕尺寸/我们划分的份数 得到了html里面的文字大小 但是不同屏幕下得到的文字大小时不一样的
3. 页面元素的rem值 = 页面元素在750元素下的px值 / html里面的文字大小值



###		2.2方案2

* flexible.js
* rem

这是手机淘宝团队退出的简洁高效,移动端适配库

它的原理是吧当前设备划分为10等份,但是在不同的设备下,比例还是一致的

我们要做的就是确定好我们当前设备的html文字大小即可


