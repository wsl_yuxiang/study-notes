##		node

Node是基于Chrome V8引擎的JavaScript代码运行环境



###		1.node.js组成

1. ECMAScript ECMAScript在node.js模块中可以使用
2. node模块API

Javascript在使用中,存在两个问题,文件依赖和命名冲突.



###		2.开发规范

1. node.js规定一个<font color=red>JavaScript文件</font>就是一个模块,模块内部<font color=red>定义的变量和函数</font>默认情况下外部无法得到
2. 模块内部可以使用<font color=red>exports对象进行成员导出</font>,使用<font color=red>require导入其他模块</font>



导入模块的时候,文件后缀名可以省略.

####		模块化成员导出的另一种方式

module.exports

当exports重新赋值的时候,以module.exports为主

###		3.系统模块

####		3.1文件模块 fs文件操作

file system 文件操作系统

```javascript
const fs = require('fs');
fs.readFile('文件路径/文件名'[,'文件编码'],callback);
```

<font color=red>node.js API中的回调函数第一个参数都是error</font>

```javascript
fs.readFile('./index.css','utf-8',(err,doc)=>{
    //如果文件读取发生错误,参数err的值为错误对象,否则err的值为null
    if(err==null){
        //文件读取正确
    }
})
```

####		3.2文件操作

1.写入文件内容

```java
fs.writeFile('文件路径','数据',callback)
```

####		3.3系统模块path 路径操作

路径拼接语法

```javascript
path.join('路径','路径')
```



文件的相对路径,相对的是node执行的相对路径

