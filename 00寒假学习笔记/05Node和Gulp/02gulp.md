##	Gulp

gulp可以做

* 项目上线,HTML CSS JS文件压缩合并
* 语法转换(es6,less..)
* 公共文件抽离
* 修改文件浏览器自动刷新



###		1.方法

1. gulp.src():获取任务要处理的文件
2. gulp.dest():输出文件
3. gulp.task():建立gulp任务
4. gulp.watch():监控文件的变化



###		2.gulp插件

* gulp-htmlmin:html文件压缩
* gulp-csso:压缩css
* gulp-babel:JavaScript语法转化
* gulp-uglify:压缩混淆JavaScript
* gulp-file-include:公共文件包含
* browsersync浏览器实时同步



1下载npm install [插件]

2引用 const csso = require('gulp-csso')

3使用csso()



###		3.package.json文件

项目描述信息,记录当前项目的信息,例如项目名称,版本,作者,github地址,当前项目依赖了哪些第三方模块

<font color='red'>npm init -y</font>命令生成package.json文件

```json
 "scripts": {
        "test": "echo \"Error: no test specified\" && exit 1",
        "build": "nodemon node.js"//别名 build
    },
//项目依赖 生产环境
"dependencies": {
    "formidable": "^1.2.2",
    "mime": "^2.4.6"
  },
//开发依赖
  "devDependencies": {
    "gulp": "^4.0.2"
  }
```

npm install  安装项目依赖和开发依赖

npm install --production 生产环境依赖



####		4.package-lock.json文件的作用

* 锁定包的版本,确保每次下载时不会因为包版本不同而产生问题
* 加快下载速度,因为该文件中已经记录了项目所依赖第三方包的树状结果和包的下载地址,



###		5.node.js中模块加载机制

```javascript
require('./find.js');
require('./find');
```

1. require方法根据模块路径查找模块,如果路径是完整路径,直接引入模块
2. 如果模块后缀省略,先找同名JS文件再找同名JS文件夹
3. 如果找到同名文件夹,找文件夹中的index.js
4. 如果找置顶的入口文件不存在或者没有指定入口文件就会报错,模块没有被找到

**模块查找规则**

```javascript
require('find');
```

1. Node.js会假设它是系统模块
2. Node.js会去node_modules文件夹中查找
3. 首先看是否用该名字的JS文件
4. 再看是否有该名字的文件夹
5. 如果是文件看里面是否有index.js
6. 如果没有index.js查看该文件夹中的package.json中的main选项确定模块入口文件





