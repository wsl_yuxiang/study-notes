##		react生命周期



![image-20220427011246818](E:\00Document\01学习\01前端学习\02学习笔记\02react\img\image-20220427011246818.png)



###		阶段1 初始化

```tsx
 // 初始化state
  constructor(props: Props) {
    super(props);
  }

  // 在页面创建好dom元素之后 挂载进页面的时候调用
  componentDidMount(){

  }
```



###		阶段2 更新

```tsx
//  在组件接收到一个新的props(更新后)时被调用
  componentWillReceiveProps(nextProps:Props){

  }
  // --> 应该使用这个 去获取更新后的props
  static getDerivedStateFromProps(nextProps:Props,nextState:State){ }
  // 组件准备更新时
  shouldComponentUpdate(nextProps:any,nextSate:any):boolean{
    return true
  }
  // 主要组件发生更新,ui重新渲染,就会发生该阶段
  componentDidUpdate(nextProps:Props,nextState:State){ }
```



###		阶段3 销毁

```ts
// 页面销毁的时候触发
  componentWillUnmount(){}
  
```

