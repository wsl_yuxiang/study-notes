##	 TypeScript基础补充

* ts是js的超集
* 给原生js添加静态类型的检查
* 与es6一样目前还不兼容(旧版本不一定能编译出es6)

## TypeScript的编译

 编译器 ``babel-loader``

编译器配置文件: <font color="red">tsconfig.json</font>



```javascript
{
  "compilerOptions": {
    // "noImplicitAny": false,// 不需要显示声明变量的类型any
    "target": "es5",// 编译后的目标
    "lib": [
      "dom",
      "dom.iterable",
      "esnext"
    ],// 库
    "allowJs": true,// 允许混合编辑js文件
    "skipLibCheck": true,
    "esModuleInterop": true,// 允许使用commonjs的方式import默认文件 如 import React from 'react'
    // "esModuleInterop": false, import * as React from 'react'
    "allowSyntheticDefaultImports": true,
    "strict": true,
    "forceConsistentCasingInFileNames": true,
    "noFallthroughCasesInSwitch": true,
    "module": "esnext",// 配置的是我们代码的模块系统,Nodejs的CommonJs es6标准的esnext,requirejs的AMD
    "moduleResolution": "node",// 决定了我们编辑器的工作方式 'node'和'classic'(19年废弃)
    "resolveJsonModule": true,
    "isolatedModules": true,//编辑器会将每个文件作为单独的模块来使用
    "noEmit": true,// 当发生错误的时候,编译器不要生成js代码
    "jsx": "react-jsx"// 允许编辑器支持编译react-jsx代码
  },
  "include": [
    "src" // 此选项列出了我们需要编译的文件 (相对或者绝对路径)
  ],
   "files":["./file1.ts"],// 这个列出编译器始终包含在编译中的文件(无论是否在exclude选项中)
   "exculde":["node_modules","**//.test.ts"] // 编译中排除的文件
}




```



1.  "noImplicitAny": false,// 不需要显示声明变量的类型any



如果不加这个的话,当你添加变量没有给类型的时候,会报错

```javascript
function App(a:any) {// 必须加上any 才不会报未声明错误
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
      </header>
    </div>
  );
}
```

2. target  编译后的目标