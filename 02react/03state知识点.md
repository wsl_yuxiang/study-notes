###		setState是同步还是异步

默认情况下是异步的 ,为了优化技能

但是react会收集一段时间内的修改数据 渲染最后一个setState

不一定是异步的,在定时器,在原生方法中,是同步的



**异步更新,同步执行**

setsState()本身不是异步的,但是对state的处理机制给人一种异步的假象,state处理一般发生在生命周期变化的时候.



###		setState原理

使用this.state = Object.assign({},old,new);



###		解决setState获取上一个值进行操作(回调地狱的问题)

```javascript
this.setState((preState,props)=>{
    return {age:preState.age+1}//获取处理之后的state
})
```



###		react生命周期

![image-20210712171927561](E:\00Document\01学习\01前端学习\02学习笔记\02react\img\image-20210712171927561.png)

###		Diff算法是如何比较的

* 只会进行同层比较
* 默认在同层比较的时候,只会进行同位置比价
* 如果比较是同类型的元素,那么就记录变化
* 如果比较的是不同类型的元素,那么久删除以前的,使用新的
* 如果上一层是不同类型的元素,那么下一层不进行更新,直接使用新的



###		列表优化

<font color="red">添加key</font>

![image-20210713111658186](E:\00Document\01学习\01前端学习\02学习笔记\02react\img\image-20210713111658186.png)