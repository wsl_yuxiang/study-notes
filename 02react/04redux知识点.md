###		redux核心思想

* 通过store保存数据
* 通过action来修改数据
* 通过reducer将store和action串联起来



![image-20210714163924627](E:\00Document\01学习\01前端学习\02学习笔记\02react\img\image-20210714163924627.png)

###		三大原则

* 单一数据源
  * 整个程序的state存在了一个store里面
* State是只读的
  * 唯一修改state的方法一定是触发action
* 使用纯函数来执行修改
  * 通过reducer将旧state和action联系在一起,并返回一个新的state



纯函数

```javascript
function sum(num1,num2){
    return num1+num2;
}
```

