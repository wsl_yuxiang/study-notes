##		1.函数组件和类组件

```javascript
<body>
    <div id="app"></div>
</body>
<script type="text/babel">
    let message = "汪苏泷"; 
    //函数组件 
    // function Home() { return ( 
        //<div>
        //<div>{message}</div>
        // <button onClick={myfun}>点击我</button> 
        // </div>) }; 
        //类组件 
        class Home1 extends React.Component{ render(){ return (
    <div>
        <div>{message}</div>
        <button onClick={myfun}>点击我1</button>
    </div>
    ) } } ReactDOM.render(
    <Home1/>,document.getElementById("app")); function myfun() { message = "汪苏泷好帅"; ReactDOM.render(
    <Home1/>,document.getElementById("app"));}
</script>
```

##		2.有状态组件和无状态组件组件

组件中的状态(state)指的就是数据

* 有状态组件指的是有自己数据的组件(逻辑组件)
* 无状态组件指的是没有自己数据的组件(展示组件)





继承于React.Component的组件,都会继承一个state属性,state属性就是专门用来保存当前数据的,都是有状态组件



##		3.this注意点

react在调用监听方法的时候,会通过apply修改监听方法的this

 在普通方法中,拿到的this时是undefined





##		4.数据驱动界面更新原理

```javascript

  //数据驱动界面更新的原理

  class myComponent extends React.Component{

    constructor(){
      super();
      this.state = null;
    }
    setState(val){
      //如果不使用setState 则不会触发ReactDOM.render
      console.log("需要更新界面啦啦");
      this.state  = val;
      ReactDOM.render(this.render(),document.getElementById("app"));
    }
  }
```



## 5 jsx 与tsx

在js项目中使用jsx

在ts项目中使用tsx

###		为什么要使用jsx

1. react不强制使用jsx
2. react认为视图的本质技术渲染逻辑与ui视图表现的内在统一
3. react把html和渲染逻辑进行了耦合,形成了jsx

### jsx的特点

* 常规html代码都可以与jsx兼容

*  可以在jsx中嵌入表达式
* 使用jsx指定子元素
* jsx命名规定 
  * 小驼峰命名
* <font color='red'>jsx会被编译为React.createElement()对象</font>



##		在jsx中防止xss攻击

![image-20220425003344060](E:\00Document\01学习\01前端学习\02学习笔记\02react\img\image-20220425003344060.png)



启用tsx 在tsconfig.json中配置

```json
开发环境下用 "jsx":"react-jsxdev"
生产环境下用 "jsx":"react-jsx"
```





##	 6. 在ts+react中使用css模块化

css in js (jss)

css在js中的应用

* 将css样式写在js文件里面



要声明css模块

```typescript
// ts 定义css的声明
declare module "*.css" {
  const css: { [key: string]: string };
  export default css;
}
```



在ts项目中使用样式属性的提示

安装``typescript-plugin-css-modules`` 在dev环境中就可以了

配置tsconfig.json

```json
"plugins": [
      {
        "name": "typescript-plugin-css-modules"
      }
    ]
```



配置.vscode>settting.json

```json
{
	"typescript.tsdk": "node_modules/typescript/lib",
	"typescript.enablePromptUseWorkspaceTsdk": true
}
```

